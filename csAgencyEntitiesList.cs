﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Xpedeon.BI.External;

namespace XSDAgencyEntitiesList
{
    public class BIXSDDescription : IBusinessObject
    {
        public DataSet Process(Dictionary<string, string> inputs, string userName)
        {
            string databaseLoginPath = Application.StartupPath;
            XmlDocument xdDataBaseInfo = new XmlDocument();
            string databaseType = string.Empty;
            string connString = string.Empty;
            try
            {
                xdDataBaseInfo.Load(databaseLoginPath + "\\DataBaseLogin.xml");
                if (xdDataBaseInfo.GetElementsByTagName("DB").Count == 0)
                {
                    return null;
                }

                if (xdDataBaseInfo.GetElementsByTagName("DataSource").Count == 0)
                {
                    return null;
                }

                if (xdDataBaseInfo.GetElementsByTagName("UserID").Count == 0)
                {
                    return null;
                }

                if (xdDataBaseInfo.GetElementsByTagName("Password").Count == 0)
                {
                    return null;
                }

                string dataSource = xdDataBaseInfo.GetElementsByTagName("DataSource")[0].InnerText;
                string userId = xdDataBaseInfo.GetElementsByTagName("UserID")[0].InnerText;
                string password = xdDataBaseInfo.GetElementsByTagName("Password")[0].InnerText;
                connString = "Data Source=" + dataSource + ";User ID=" + userId + ";Password=" + password;
            }
            catch (FileNotFoundException)
            {
                return null;
            }

            databaseType = (xdDataBaseInfo.GetElementsByTagName("DB"))[0].InnerText;
            DbDataAdapter dbAdapter = null;
            DbCommand dbCommand = null;
            DbConnection dbConn = null;
            DbProviderFactory objFactory = null;

            objFactory = SqlClientFactory.Instance;
            dbConn = new SqlConnection(connString);

            dbAdapter = objFactory.CreateDataAdapter();
            dbCommand = objFactory.CreateCommand();
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.Connection = dbConn;
            dbCommand.CommandText = "Utility.BIAgencyEntities";

            //DbParameter parameter = objFactory.CreateParameter();

            //parameter = objFactory.CreateParameter();
            //parameter.ParameterName = "@IV_XE_USERNAME";
            //parameter.DbType = DbType.String;
            //parameter.Size = 30;
            //parameter.Value = userName;
            //dbCommand.Parameters.Add(parameter);

            dbAdapter.SelectCommand = dbCommand;
            dbAdapter.SelectCommand.CommandTimeout = 0;

            dsAgencyEntitiesList ds = new dsAgencyEntitiesList();

            DataTable dt = new DataTable();

            try
            {
                dbConn.Open();
                dbAdapter.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {

                if (dbConn.State == ConnectionState.Open)
                {
                    dbConn.Close();
                }
            }

            if (dt != null)
            {
                ds.dtAgencyEntitiesList.Merge(dt.DefaultView.ToTable());
            }

            objFactory = null;
            dbAdapter.Dispose();
            dbCommand.Dispose();
            return ds;
        }

    }
    public class InputXSDDescription : IInput
    {
        public DataSet Process(Dictionary<string, string> inputs, string userName, Dictionary<string, string> defaultValues)
        {
            string databaseLoginPath = Application.StartupPath;
            XmlDocument xdDataBaseInfo = new XmlDocument();
            string databaseType = string.Empty;
            string connString = string.Empty;
            try
            {
                xdDataBaseInfo.Load(databaseLoginPath + "\\DataBaseLogin.xml");
                if (xdDataBaseInfo.GetElementsByTagName("DB").Count == 0)
                {
                    return null;
                }

                if (xdDataBaseInfo.GetElementsByTagName("DataSource").Count == 0)
                {
                    return null;
                }

                if (xdDataBaseInfo.GetElementsByTagName("UserID").Count == 0)
                {
                    return null;
                }

                if (xdDataBaseInfo.GetElementsByTagName("Password").Count == 0)
                {
                    return null;
                }

                string dataSource = xdDataBaseInfo.GetElementsByTagName("DataSource")[0].InnerText;
                string userId = xdDataBaseInfo.GetElementsByTagName("UserID")[0].InnerText;
                string password = xdDataBaseInfo.GetElementsByTagName("Password")[0].InnerText;
                connString = "Data Source=" + dataSource + ";User ID=" + userId + ";Password=" + password;
            }
            catch (FileNotFoundException)
            {
                return null;
            }

            databaseType = (xdDataBaseInfo.GetElementsByTagName("DB"))[0].InnerText;
            DbDataAdapter dbAdapter = null;
            DbCommand dbCommand = null;
            DbConnection dbConn = null;
            DbProviderFactory objFactory = null;

            objFactory = SqlClientFactory.Instance;
            dbConn = new SqlConnection(connString);

            dbAdapter = objFactory.CreateDataAdapter();
            dbCommand = objFactory.CreateCommand();
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.Connection = dbConn;
            dbCommand.CommandText = "Utility.BIAgencyEntities";

            //DbParameter parameter = objFactory.CreateParameter();

            //parameter = objFactory.CreateParameter();
            //parameter.ParameterName = "@IV_XE_USERNAME";
            //parameter.DbType = DbType.String;
            //parameter.Size = 30;
            //parameter.Value = userName;
            //dbCommand.Parameters.Add(parameter);

            dbAdapter.SelectCommand = dbCommand;
            dbAdapter.SelectCommand.CommandTimeout = 0;

            dsAgencyEntitiesList ds = new dsAgencyEntitiesList();

            DataTable dt = new DataTable();

            try
            {
                dbConn.Open();
                dbAdapter.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {

                if (dbConn.State == ConnectionState.Open)
                {
                    dbConn.Close();
                }
            }

            if (dt != null)
            {
                ds.dtAgencyEntitiesList.Merge(dt.DefaultView.ToTable());
            }

            objFactory = null;
            dbAdapter.Dispose();
            dbCommand.Dispose();
            return ds;
        }

    }
}
